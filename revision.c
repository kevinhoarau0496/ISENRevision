/**
 * \file revision.c
 * \brief Programme de révision.
 * \author Kevin HOARAU
 * \version 1.0
 * \date 02 mai 2017
 *
 * Programme regroupant toutes les techniques vues en cours afin de se préparer au partiel.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRMAX 100
#define TMAX 50
#define PANIERDEF 10
#define ERREURALLOC "ERREUR : Impossible d'allouer de la mémoire !\n"

#define cleanBuffer() while(getchar() != '\n');

//////////DEFINITION DES STRUCTURES//////////
/**
 * \struct SPERSONNE
 * \brief Objet Personne.
 *
 * Personne est une structure regroupant les informations basiques d'une personne: 
 * le nom ; le prénom ; l'age ; le panier de la personne et
 * la personne suivante dans la liste chainée
 */
typedef struct SPERSONNE
{
	char nom[STRMAX];
	char prenom[STRMAX];
	int age;

	struct SPERSONNE * personne_suiv;
	struct SARTICLE * panier;
} Personne;

/**
 * \struct SARTICLE
 * \brief Objet Article.
 *
 * Article est une structure regroupant les informations basiques d'un article : 
 * le libelle ; le code ; le prix et
 * l'article suivant dans la liste chainée
 */
typedef struct SARTICLE
{
	char libelle[STRMAX];
	int code;
	int prix;

	struct SARTICLE * article_suiv;
} Article;

/**
 * \struct SRAYON
 * \brief Objet Rayon.
 *
 * Rayon est une structure regroupant les informations permettant
 * la création d'un arbre binaire
 */
typedef struct SRAYON
{
	Article * article;

	struct SRAYON * gauche;
	struct SRAYON * droite;
} Rayon;

/**
 * \struct SMAGASIN
 * \brief Objet Magasin.
 *
 * Magasin est une structure regroupant les informations basiques d'un magasin : 
 * le nom ; l'adresse ; le code ; ses rayons et
 * le magasin suivant dans la liste chainée
 */
typedef struct SMAGASIN
{
	char nom[STRMAX];
	char adresse[STRMAX];
	int code;

	struct SMAGASIN * magasin_suiv;
	Rayon * rayon;
} Magasin;

/**
 * \struct SBDDCOMMERCE
 * \brief Objet BDDCommerce.
 *
 * BDDCommerce est une structure regroupant les informations d'un commerce : 
 * un tableau de code d'article, un tableau de code de magasin ainsi que
 * le nombre de code actuel d'article(maxCodeArticle) et de magasin(maxCodeMagasin)
 */
typedef struct SBDDCOMMERCE
{
	int * codeMagasin;
	int maxCodeMagasin;

	int * codeArticle;
	int maxCodeArticle;
} BDDCommerce;

/**
 * \struct SDONNEE
 * \brief Objet Donnee.
 *
 * Donnee est une structure regroupant les informations nécessaire à l'execution du programme : 
 * La liste chainée de personnes, de magasins, d'articles ainsi que
 * la base de donnée du commerce
 */
typedef struct SDONNEE
{
	Personne * personnes;
	Magasin * magasins;
	Article * articles;
	BDDCommerce * bdd;
} Donnee;

//////////PROTOYPE DES FONCTIONS//////////
/* PERSONNE */
Personne * creerPersonne();
Personne * ajoutPersonneDansChaine(Personne * teteDeListe, Personne * nouvellePersonne);
void supprimerPersonnes(Personne * personne);
void afficherPersonne(Personne * personne, int recursif, int panier);
Personne * recherchePersonne(Personne * personnes);
void passageEnCaisse(Personne * personne);
/* ARTICLE */
Article * creerArticle();
Article * ajoutArticleDansChaine(Article * teteDeListe, Article * nouvelArticle);
void supprimerArticles(Article * article);
void afficherArticle(Article * article, int recursif);
Article * rechercheArticle(Article * articles);
void ajoutArticleDansPanier(Personne * personne, Article * article);
int paimentDuProchainArticle(Personne * personne);
void verifierArticle(BDDCommerce * bdd);
/* MAGASIN */
Magasin * creerMagasin();
Magasin * ajoutMagasinDansLaChaine(Magasin * teteDeListe, Magasin * nouveauMagasin);
void supprimerMagasins(Magasin * magasin);
void afficherMagasin(Magasin * magasin, int recursif);
Magasin * rechercheMagasin(Magasin * magasins);
void verifierMagasin(BDDCommerce * bdd);
/* RAYON */
Rayon * creerRayon(Article * article, Rayon * gauche, Rayon * droite);
Rayon * insererRayon(Rayon * rayon, Article * article);
void afficherRayon(Rayon * rayon, int niveauActuel, int niveauMax);
int nombreDeRayon(Rayon * rayon);
/* BDD */
BDDCommerce * creerBase();
void supprimerBase(BDDCommerce * bdd);
void ajouterArticleDansBase(BDDCommerce * bdd, Article * article);
void ajouterMagasinDansBase(BDDCommerce * bdd, Magasin * magasin);
/* QUICKSORT */
int perscmp(Personne * a, Personne * b);
Personne * teteDeListe(Personne * personne);
Personne * resteDeListe(Personne * personne);
Personne * concatListe(Personne * gauche, Personne * droite);
Personne * quickSort(Personne * p);
/* DICHOTOMIE */
int rechercheCode(int * base, int min, int max, int quoi);
/* TRI */
void triBase(int * base, int max);
/* PROGRAMME */
Donnee * initialisationProgramme();
void arretProgramme(Donnee * donnee);
void afficherMenuPrincipal(Donnee * donnee);
void afficherMenuPersonne(Donnee * donnee);
void afficherMenuArticle(Donnee * donnee);
void afficherMenuMagasin(Donnee * donnee);

//////////GESTIONS DES PERSONNES//////////
/**
 * \fn Personne * creerPersonne()
 * \brief Fonction de création d'une nouvelle instance d'un objet Personne.
 *
 * \return Instance nouvelle allouée d'un objet de type Personne ou NULL.
 */
Personne * creerPersonne()
{
	Personne * nouvellePersonne = (Personne *) malloc(sizeof(Personne));
	if(nouvellePersonne == NULL)
	{
		printf(ERREURALLOC);
		return NULL;
	}

	//Remplissage des champs de la structure "Personne"
	printf("Création d'une nouvelle personne !\n");
	printf("Nom de la personne : ");
	scanf("%s", nouvellePersonne->nom);
	printf("Prenom de la personne : ");
	scanf("%s", nouvellePersonne->prenom);
	printf("Age de la personne : ");
	scanf("%d", &nouvellePersonne->age);
	nouvellePersonne->personne_suiv = NULL;

	//Initialisation du panier de la personne
	nouvellePersonne->panier = NULL;

	//On retourne la nouvelle personne créée
	return nouvellePersonne;
}

/**
 * \fn Personne * ajoutPersonneDansChaine(Personne * teteDeListe, Personne * nouvellePersonne)
 * \brief Fonction d'ajout d'une nouvelle Personne à la liste chainée.
 *
 * \param teteDeListe Tete de la liste chainée de Personne.
 * \param nouvellePersonne La nouvelle Personne créée précédemment.
 * \return NULL si nouvellePersonne vaut NULL, sinon la nouvelle tête de la chaine.
 */
Personne * ajoutPersonneDansChaine(Personne * teteDeListe, Personne * nouvellePersonne)
{
	if(nouvellePersonne == NULL)
		return NULL;

	nouvellePersonne->personne_suiv = teteDeListe;
	return nouvellePersonne;
}

/**
 * \fn void supprimerPersonnes(Personne * personne)
 * \brief Fonction de suppression de toutes les Personnes de la liste chainée.
 *
 * \param personne Tete de la liste chainée de Personne.
 */
void supprimerPersonnes(Personne * personne)
{
	if(personne == NULL)
		return;

	
	supprimerPersonnes(personne->personne_suiv);
	supprimerArticles(personne->panier);
	free(personne);
	personne = NULL;
}

/**
 * \fn void afficherPersonne(Personne * personne, int recursif, int panier)
 * \brief Fonction d'affichage de la liste chainée de Personne.
 *
 * \param personne Tete de la liste chainée de Personne.
 * \param recursif 0 ou 1 determinant l'appel récursif de la fonction.
 * \param panier 0 ou 1 determinant l'appel de la fonction d'affichage du panier.
 */
void afficherPersonne(Personne * personne, int recursif, int panier)
{
	if(personne == NULL)
		return;

	printf("Personne : %s %s - %dans\n", personne->prenom, personne->nom, personne->age);

	if(panier)
	{
		if(personne->panier == NULL)
			printf("Panier : vide\n");
		else
		{
			printf("Panier :\n");
			afficherArticle(personne->panier, 1);
		}
	}

	if(recursif)
		afficherPersonne(personne->personne_suiv, recursif, panier);
}

/**
 * \fn Personne * recherchePersonne(Personne * personnes)
 * \brief Fonction permettant la recherche d'une Personne dans une liste chainée de Personne.
 *
 * \param personnes Tete de la liste chainée de Personne.
 * \return la Personne trouvée, sinon NULL.
 */
Personne * recherchePersonne(Personne * personnes)
{
	if(personnes == NULL)
	{
		printf("ERREUR : Aucune personne présente dans la liste\n");
		return NULL;
	}

	char nom[STRMAX];
	char prenom[STRMAX];
	printf("Prénom de la personne : ");
	scanf("%s", prenom);
	printf("Nom de la personne : ");
	scanf("%s", nom);

	for(Personne * tmp = personnes ; tmp != NULL ; tmp = tmp->personne_suiv)
		if(!strcmp(nom, tmp->nom) && !strcmp(prenom, tmp->prenom))
			return tmp;

	printf("La personne \"%s %s\" n'est pas trouvable dans la liste des personnes !\n", prenom, nom);
	return NULL;
}

/**
 * \fn void passageEnCaisse(Personne * personne)
 * \brief Fonction permettant le passage en caisse d'une personne
 *
 * Le principe est d'appeler la fonction de dépilement du panier (Le panier est une FIFO).
 * Une phrase de conclusion sera affichée à la fin pour dire combien a coûté le panier de la personne.
 * \param personne Une instance de la structure Personne (!= NULL).
 */
void passageEnCaisse(Personne * personne)
{
	if(personne == NULL)
	{
		printf("ERREUR : Impossible de faire passer la personne à la caisse !\n");
		return;
	}

	printf("Passage en caisse de la personne : %s %s\n", personne->prenom, personne->nom);

	int prixTotal = 0;
	int prix;
	while((prix = paimentDuProchainArticle(personne)) != 0)
		prixTotal += prix;

	printf("Frais total lors du passage en caisse de la personne : %d€\n", prixTotal);
}

//////////GESTIONS DES ARTICLES//////////
/**
 * \fn Article * creerArticle()
 * \brief Fonction de création d'une nouvelle instance d'un objet Article.
 *
 * \return Instance nouvelle allouée d'un objet de type Article ou NULL.
 */
Article * creerArticle()
{
	Article * nouvelArticle = (Article *) malloc(sizeof(Article));
	if(nouvelArticle == NULL)
	{
		printf(ERREURALLOC);
		return NULL;
	}

	//Remplissage des champs de la structure "Article"
	printf("Création d'un nouvel article !\n");
	printf("Nom de l'article : ");
	scanf("%s", nouvelArticle->libelle);

	nouvelArticle->code = 0;
	do
	{
		printf("Code de l'article (>= 0) : ");
		scanf("%d", &nouvelArticle->code);
	} while(nouvelArticle->code < 0);
	
	nouvelArticle->prix = 0;
	do
	{
		printf("Prix de l'article n°%d (>= 0) : ", nouvelArticle->code);
		scanf("%d", &nouvelArticle->prix);
	} while(nouvelArticle->prix < 0);

	nouvelArticle->article_suiv = NULL;
	
	//On retourne le nouvel article crée
	return nouvelArticle;
}

/**
 * \fn Article * ajoutArticleDansChaine(Article * teteDeListe, Article * nouvelArticle)
 * \brief Fonction d'ajout d'un nouvel Article à la liste chainée.
 *
 * \param teteDeListe Tete de la liste chainée d'Article.
 * \param nouvelArticle Le nouvel Article créée précédemment.
 * \return NULL si nouvelArticle vaut NULL, sinon la nouvelle tête de la chaine.
 */
Article * ajoutArticleDansChaine(Article * teteDeListe, Article * nouvelArticle)
{
	if(nouvelArticle == NULL)
		return NULL;

	nouvelArticle->article_suiv = teteDeListe;
	return nouvelArticle;
}

/**
 * \fn void supprimerArticles(Article * article, int recursif)
 * \brief Fonction de suppression de tous les Articles de la liste chainée.
 *
 * \param article Tete de la liste chainée d'Article.
 */
void supprimerArticles(Article * article)
{
	if(article == NULL)
		return;

	supprimerArticles(article->article_suiv);

	free(article);
	article = NULL;
}

/**
 * \fn void afficherArticle(Article * article, int recursif)
 * \brief Fonction d'affichage de la liste chainée d'Article.
 *
 * \param article Tete de la liste chainée d'Article.
 * \param recursif 0 ou 1 determinant l'appel récursif de la fonction.
 */
void afficherArticle(Article * article, int recursif)
{
	if(article == NULL)
		return;

	printf("Article n°%d : %s au prix de %d€\n", article->code, article->libelle, article->prix);

	if(recursif)
		afficherArticle(article->article_suiv, recursif);
}

/**
 * \fn Article * rechercheArticle(Article * article)
 * \brief Fonction permettant la recherche d'un Article dans une liste chainée d'Article.
 *
 * \param articles Tete de la liste chainée d'Article.
 * \return l'Article trouvé, sinon NULL.
 */
Article * rechercheArticle(Article * articles)
{
	if(articles == NULL)
	{
		printf("ERREUR : Aucun article présent dans la liste\n");
		return NULL;
	}

	int codeArticle;
	printf("Code de l'article : ");
	scanf("%d", &codeArticle);

	for(Article * tmp = articles ; tmp != NULL ; tmp = tmp->article_suiv)
		if(codeArticle == tmp->code)
			return tmp;

	printf("L'article n°%d n'est pas trouvable dans la liste des articles !\n", codeArticle);
	return NULL;
}

/**
 * \fn void ajoutArticleDansPanier(Personne * personne, Article * article)
 * \brief Fonction d'empilement d'Article dans le panier d'une Personne
 *
 * Ici, il s'agit de la fonction d'empilement.
 * Premièrement, on vérifie si la Personne ou l'Article en question ne valent pas NULL
 * Puis il est nécessaire de créer une copie de l'article pour l'ajouter au panier puisque l'article passé en paramètre peut être utilisé ailleurs.
 * Enfin, on parcourt le panier de la Personne (correspondant à une chaine), afin d'ajouter la copie de l'article à la fin.
 * \param personne Une instance de la structure Personne (!= NULL).
 * \param article Une instance de la structure Article (!= NULL).
 */
void ajoutArticleDansPanier(Personne * personne, Article * article)
{
	if(personne == NULL || article == NULL)
	{
		printf("ERREUR : Ajout de l'article dans le panier impossible\n");
		return;
	}

	Article * copieArticle = (Article *) malloc(sizeof(Article));
	if(copieArticle == NULL)
	{
		printf(ERREURALLOC);
		return;
	}
	strcpy(copieArticle->libelle, article->libelle);
	copieArticle->code = article->code;
	copieArticle->prix = article->prix;
	copieArticle->article_suiv = NULL;

	if(personne->panier != NULL)
	{
		Article * articleActuel = personne->panier;
		while(articleActuel->article_suiv != NULL)
			articleActuel = articleActuel->article_suiv;

		articleActuel->article_suiv = copieArticle;
	}
	else
		personne->panier = copieArticle;

	printf("La personne %s %s met l'article \"%s\" dans son panier\n", personne->prenom, personne->nom, article->libelle);
}

/**
 * \fn int paimentDuProchainArticle(Personne * personne)
 * \brief Fonction de dépilement d'Article dans le panier d'une Personne
 *
 * Ici, il s'agit de la fonction de dépilement.
 * Premièrement, on parcourt le panier(La liste) jusqu'à arriver au dernier Article.
 * Puis on libère les ressources utilisées.
 * Et on retourne le prix de l'article.
 * \param personne Une instance de la structure Personne (!= NULL).
 * \return prix Le prix de l'article dépilé.
 */
int paimentDuProchainArticle(Personne * personne)
{
	int prix = 0;
	if(personne->panier != NULL)
	{
		Article * article = personne->panier;
		prix = article->prix;
		personne->panier = article->article_suiv;

		free(article);
		article = NULL;
	}

	return prix;
}

/**
 * \fn void verifierArticle(BDDCommerce * bdd)
 * \brief Fonction permettant la vérification de la création d'un Article.
 *
 * Ici, il s'agit d'une fonction implémentant un tri et une recherche par dichotomie.
 * Premièrement, on vérifie si la Base de donnée est créée et que le nombre d'Article crée est différent de 0.
 * Puis, on demande à l'utilisateur d'entrer le code de l'Article à chercher.
 * On effectue un tri à bulle afin de ranger de manière croissante les codes d'Articles dans la base de donnée.
 * Après, on effectue une recherche par dichotomie dans la base de donnée afin de vérifier l'existance du code de l'Article et donc de l'Article lui-même.
 * Enfin, suivant le résultat on affiche une phrase de conclusion.
 * \param bdd Une instance d'une Base de Donnée de commerce (!= NULL).
 */
void verifierArticle(BDDCommerce * bdd)
{
	if(bdd == NULL || bdd->maxCodeArticle == 0)
	{
		printf("ERREUR : Aucun article présent dans la base de donnée\n");
		return;
	}

	int quoi;
	printf("Code de l'article à vérifier : ");
	scanf("%d", &quoi);

	triBase(bdd->codeArticle, bdd->maxCodeArticle);
	int resultat = rechercheCode(bdd->codeArticle, 0, bdd->maxCodeArticle, quoi);

	if(quoi == bdd->codeArticle[resultat])
		printf("L'article est présent dans la base de donnée !\n");
	else
		printf("L'article n'est pas présent dans la base de donnée !\n");

}

//////////GESTIONS DES MAGASINS//////////
/**
 * \fn Magasin * creerMagasin()
 * \brief Fonction de création d'une nouvelle instance d'un objet Magasin.
 *
 * \return Instance nouvelle allouée d'un objet de type Magasin ou NULL.
 */
Magasin * creerMagasin()
{
	Magasin * nouveauMagasin = (Magasin *) malloc(sizeof(Magasin));
	if(nouveauMagasin == NULL)
	{
		printf(ERREURALLOC);
		return NULL;
	}

	//Remplissage des champs de la structure "Magasin"
	printf("Création d'un nouveau magasin !\n");
	printf("Nom du magasin : ");
	scanf("%s", nouveauMagasin->nom);
	printf("Adresse du magasin : ");
	cleanBuffer();
	fgets(nouveauMagasin->adresse, STRMAX, stdin);
	printf("Code du magasin : ");
	scanf("%d", &nouveauMagasin->code);

	nouveauMagasin->magasin_suiv = NULL;
	nouveauMagasin->rayon = NULL;

	return nouveauMagasin;
}

/**
 * \fn Magasin * ajoutMagasinDansLaChaine(Magasin * teteDeListe, Magasin * nouveauMagasin)
 * \brief Fonction d'ajout d'un nouveau Magasin à la liste chainée.
 *
 * \param teteDeListe Tete de la liste chainée de Magasin.
 * \param nouveauMagasin Le nouveau Magasin créée précédemment.
 * \return NULL si nouveauMagasin vaut NULL, sinon la nouvelle tête de la chaine.
 */
Magasin * ajoutMagasinDansLaChaine(Magasin * teteDeListe, Magasin * nouveauMagasin)
{
	if(nouveauMagasin == NULL)
		return NULL;

	nouveauMagasin->magasin_suiv = teteDeListe;
	return nouveauMagasin;
}

/**
 * \fn void supprimerMagasins(Magasin * magasin, int recursif)
 * \brief Fonction de suppression de tous les Magasins de la liste chainée.
 *
 * \param magasin Tete de la liste chainée de Magasin.
 */
void supprimerMagasins(Magasin * magasin)
{
	if(magasin == NULL)
		return;

	supprimerMagasins(magasin->magasin_suiv);

	free(magasin);
	magasin = NULL;
}

/**
 * \fn void afficherMagasin(Magasin * magasin, int recursif)
 * \brief Fonction d'affichage de la liste chainée de Magasin.
 *
 * \param magasin Tete de la liste chainée de Magasin.
 * \param recursif 0 ou 1 determinant l'appel récursif de la fonction.
 */
void afficherMagasin(Magasin * magasin, int recursif)
{
	if(magasin == NULL)
		return;

	printf("Magasin n°%d: %s\nAdresse : %s", magasin->code, magasin->nom, magasin->adresse);
	printf("Articles disponibles : \n");
	//TODO

	printf("----------------------\n");
	if(recursif)
		afficherMagasin(magasin->magasin_suiv, recursif);
}

/**
 * \fn Magasin * rechercheMagasin(Magasin * magasin)
 * \brief Fonction permettant la recherche d'un Magasin dans une liste chainée de Magasin.
 *
 * \param magasins Tete de la liste chainée de Magasin.
 * \return le Magasin trouvé, sinon NULL.
 */
Magasin * rechercheMagasin(Magasin * magasins)
{
	if(magasins == NULL)
	{
		printf("ERREUR : Aucun magasin présent dans la liste\n");
		return NULL;
	}

	int codeMagasin;
	printf("Code du magasin : ");
	scanf("%d", &codeMagasin);

	for(Magasin * tmp = magasins ; tmp != NULL ; tmp = tmp->magasin_suiv)
		if(codeMagasin == tmp->code)
			return tmp;

	printf("Le magasin n°%d n'est pas trouvable dans la liste des magasins !\n", codeMagasin);
	return NULL;
}

/**
 * \fn void verifierMagasin(BDDCommerce * bdd)
 * \brief Fonction permettant la vérification de la création d'un Magasin.
 *
 * Ici, il s'agit d'une fonction implémentant un tri et une recherche par dichotomie.
 * Premièrement, on vérifie si la Base de donnée est créée et que le nombre de Magasin crée est différent de 0.
 * Puis, on demande à l'utilisateur d'entrer le code du Magasin à chercher.
 * On effectue un tri à bulle afin de ranger de manière croissante les codes de Magasin dans la base de donnée.
 * Après, on effectue une recherche par dichotomie dans la base de donnée afin de vérifier l'existance du code du Magasin et donc du Magasin lui-même.
 * Enfin, suivant le résultat on affiche une phrase de conclusion.
 * \param bdd Une instance d'une Base de Donnée de commerce (!= NULL).
 */
void verifierMagasin(BDDCommerce * bdd)
{
	if(bdd == NULL || bdd->maxCodeMagasin == 0)
	{
		printf("ERREUR : Aucun magasin présent dans la base de donnée\n");
		return;
	}

	int quoi;
	printf("Code du magasin à vérifier : ");
	scanf("%d", &quoi);

	triBase(bdd->codeMagasin, bdd->maxCodeMagasin);
	int resultat = rechercheCode(bdd->codeMagasin, 0, bdd->maxCodeMagasin, quoi);

	if(quoi == bdd->codeMagasin[resultat])
		printf("Le magasin est présent dans la base de donnée !\n");
	else
		printf("Le magasin n'est pas présent dans la base de donnée !\n");

}

//////////GESTIONS DES RAYONS//////////
/**
 * \fn Rayon * creerRayon(Article * article, Rayon * gauche, Rayon * droite)
 * \brief Fonction permettant de créer un noeud de l'arbre binaire.
 *
 * Un Rayon est composé ainsi :
 * La racine est un article
 * Le fils de gauche est un article avec un prix < à l'article de la racine.
 * Le fils de droite est un article avec un prix > à l'article de la racine.
 * L'ensemble forme un Noeud nommé : Rayon

 *		 Article
 *		  /   \
 *	 Gauche   Droite
 *
 * \param article Une instance de la structure Article (!= NULL).
 * \param gauche Une instance de la structure Rayon.
 * \param droite Une instance de la structure Rayon.
 * \return Le nouveau noeud créé et initialisé, ou NULL si erreur d'allocation mémoire.
 */
Rayon * creerRayon(Article * article, Rayon * gauche, Rayon * droite)
{
	Rayon * rayon = (Rayon *) malloc(sizeof(Rayon));
	if(article == NULL || rayon == NULL)
	{
		printf(ERREURALLOC);
		return NULL;
	}

	rayon->article = article;
	rayon->gauche = gauche;
	rayon->droite = droite;

	return rayon;
}

/**
 * \fn Rayon * insererRayon(Rayon * rayon, Article * article)
 * \brief Fonction permettant d'insérer à la bonne place l'Article dans l'arbre binaire de manière récursive.
 *
 * La fonction va insérer l'Article passé en paramètre à la bonne place dans l'arbre binaire en comparant son prix. 
 * \param rayon La racine de l'arbre binaire.
 * \param article Une instance de la structure Article (!= NULL).
 * \return Le nouvel arbre binaire.
 */
Rayon * insererRayon(Rayon * rayon, Article * article)
{
	if(rayon == NULL)
		return creerRayon(article, NULL, NULL);
	else
	{
		if(article->prix < rayon->article->prix)
			rayon->gauche = insererRayon(rayon->gauche, article);
		else
			rayon->droite = insererRayon(rayon->droite, article);	
		return rayon;
	}
}

/**
 * \fn void afficherRayon(Rayon * rayon, int niveauActuel, int niveauMax)
 * \brief Fonction permettant d'afficher de manière récursive l'arbre binaire
 *
 * Ici l'affichage de l'abre binaire s'effectue de la gauche vers la droite, l'extême gauche étant la racine principale.
 *
 *				    Article
 *		   Article
 *				    Article
 * Article
 *				    Article
 *		   Article
 *			        Article
 *
 * \param rayon La racine de l'arbre binaire à partir duquel afficher.
 * \param niveauActuel Le niveau actuel dans l'arbre binaire à partir duquel afficher.
 * \param niveauMax Le niveau max de l'arbre binaire à ne pas dépasser pour l'affichage.
 */
void afficherRayon(Rayon * rayon, int niveauActuel, int niveauMax)
{
	if(rayon != NULL)
	{
    	afficherRayon(rayon->droite, niveauActuel+1, niveauMax);
    	for (int i = 0 ; i < niveauActuel ; ++i)
      		printf("    ");

    	printf("%d€\n", rayon->article->prix);
    	afficherRayon(rayon->gauche, niveauActuel+1, niveauMax);
  	}
  	else
  	{
    	if (niveauActuel < niveauMax)
    	{
      		afficherRayon(NULL, niveauActuel + 1, niveauMax);

      		for (int i = 0 ; i < niveauActuel ; ++i)
        		printf("    ");
      		printf("..\n");

      		afficherRayon(NULL, niveauActuel + 1, niveauMax);
    	}
  	}
}

/**
 * \fn int nombreDeRayon(Rayon * rayon)
 * \brief Fonction permettant d'avoir le nombre d'étage de l'arbre binaire.
 *
 * Si la racine passée en paramètre vaut NULL alors il n'y a aucun étage.
 * \param rayon La racine à partir de laquelle compté le nombre d'étage.
 * \return le nombre d'étage de l'arbre.
 */
int nombreDeRayon(Rayon * rayon)
{
	if(rayon == NULL)
		return 0;

	return (1 + nombreDeRayon(rayon->gauche) + nombreDeRayon(rayon->droite));
}

//////////GESTION DE LA BASE//////////
/**
 * \fn BDDCommerce * creerBase()
 * \brief Fonction de création d'une nouvelle instance d'un objet BDDCommerce.
 *
 * \return Instance nouvelle allouée d'un objet de type BDDCommerce ou NULL.
 */
BDDCommerce * creerBase()
{
	BDDCommerce * bdd = (BDDCommerce *) malloc(sizeof(BDDCommerce));
	if(bdd == NULL)
	{
		printf(ERREURALLOC);
		return NULL;
	}

	bdd->codeArticle = (int *) malloc(sizeof(int));
	bdd->codeMagasin = (int *) malloc(sizeof(int));
	bdd->maxCodeArticle = 0;
	bdd->maxCodeMagasin = 0;

	return bdd;
}

/**
 * \fn void supprimerBase(BDDCommerce * bdd)
 * \brief Fonction de suppression d'une instance d'un objet BDDCommerce.
 *
 * Cette fonction a pour but de libérer toutes les ressources créées dans la Base de donnée de commerce.
 * \param bdd Une instance d'une Base de Donnée de commerce (!= NULL).
 */
void supprimerBase(BDDCommerce * bdd)
{
	if(bdd == NULL)
		return;

	free(bdd->codeArticle);
	free(bdd->codeMagasin);
	free(bdd);
	bdd = NULL;
}

/**
 * \fn void ajouterArticleDansBase(BDDCommerce * bdd, Article * article)
 * \brief Fonction d'ajout du code d'un Article dans la Base de donnée de commerce.
 *
 * Cette fonction a pour but d'insérer le code de l'article dans le tableau d'int associé et alloué dynamiquement, à la position suivante.
 * Puis, incrémenter de 1 la position afin de préparer la prochaine insertion.
 * Enfin, effectuer un realloc puisqu'il s'agit d'un tableau alloué dynamiquement.
 * \param bdd Une instance d'une Base de Donnée de commerce (!= NULL).
 * \param article Une instance d'un Article (!= NULL).
 */
void ajouterArticleDansBase(BDDCommerce * bdd, Article * article)
{
	bdd->codeArticle[bdd->maxCodeArticle] = article->code;
	bdd->maxCodeArticle += 1;
	bdd->codeArticle = realloc(bdd->codeArticle, sizeof(int) * bdd->maxCodeArticle);
}

/**
 * \fn void ajouterMagasinDansBase(BDDCommerce * bdd, Magasin * magasin)
 * \brief Fonction d'ajout du code d'un Magasin dans la Base de donnée de commerce.
 *
 * Cette fonction a pour but d'insérer le code du Magasin dans le tableau d'int associé et alloué dynamiquement, à la position suivante.
 * Puis, incrémenter de 1 la position afin de préparer la prochaine insertion.
 * Enfin, effectuer un realloc puisqu'il s'agit d'un tableau alloué dynamiquement.
 * \param bdd Une instance d'une Base de Donnée de commerce (!= NULL).
 * \param magasin Une instance d'un Magasin (!= NULL).
 */
void ajouterMagasinDansBase(BDDCommerce * bdd, Magasin * magasin)
{
	bdd->codeMagasin[bdd->maxCodeMagasin] = magasin->code;
	bdd->maxCodeMagasin += 1;
	bdd->codeMagasin = realloc(bdd->codeMagasin, sizeof(int) * bdd->maxCodeMagasin);
}

//////////QUICKSORT//////////
/**
 * \fn int perscmp(Personne * a, Personne * b)
 * \brief Fonction permettant la comparaison de deux Personnes grâce à l'attribut "age" de la structure.
 *
 * Cette fonction de comparaison est utilisé dans le QuickSort.
 * \param a Une instance d'une Personne (!= NULL).
 * \param b Une instance d'une Personne (!= NULL).
 * \return 1 si a>b, -1 si a<b, 0 sinon.
 */
int perscmp(Personne * a, Personne * b)
{
	if(a->age > b->age)
		return 1;
	else if(a->age < b->age)
		return -1;
	else
		return 0;
}

/**
 * \fn Personne * teteDeListe(Personne * personne)
 * \brief Fonction permettant d'obtenir une copie de la tête d'une liste chainée de Personne.
 *
 * Cette fonction créer une copie de la tete d'une liste chainée, en recopiant correctement les champs de la structure.
 * Sans oublier de mettre le maillon suivant à NULL !
 * \param personne La tête d'une liste chainé de Personne.
 * \return Une copie de la tête de la liste, NULL si erreur d'allocation mémoire.
 */
Personne * teteDeListe(Personne * personne)
{
	Personne * p = (Personne *) malloc(sizeof(Personne));
	if(p == NULL)
	{
		printf(ERREURALLOC);
		return NULL;
	}

	strcpy(p->nom, personne->nom);
	strcpy(p->prenom, personne->prenom);
	p->age = personne->age;
	p->panier = personne->panier;
	p->personne_suiv = NULL;

	return p;
}

/**
 * \fn Personne * resteDeListe(Personne * personne)
 * \brief Fonction permettant d'obtenir une liste chainée sans la tête.
 *
 * Cette fonction permet de séparer la tête de la liste chainée du reste de la liste.
 * Evidemment il faut libérer les ressources de la tête.
 * \param personne La tête d'une liste chainé de Personne.
 * \return Le reste de la liste chainée sans la tête.
 */
Personne * resteDeListe(Personne * personne)
{
	Personne * reste = personne->personne_suiv;
	free(personne);
	personne = NULL;
	return reste;
}

/**
 * \fn Personne * concatListe(Personne * gauche, Personne * droite)
 * \brief Fonction permettant de coller la liste chainée droite à la suite de la liste chainée gauche.
 *
 * Si la liste chainé gauche vaut NULL alors on retourne directement la liste chainée droite puisqu'il est inutile de la coller à la gauche.
 * Sinon, on créé une variable temporaire de type Personne permettant de parcourir la liste chainée de gauche afin d'acceder au dernier élément.
 * Enfin, il suffit de dire que l'élément suivant du dernier élément de la liste chainée de gauche est égale à la liste chainée de droite. 
 * \param gauche La tête d'une liste chainé de Personne.
 * \param droite La tête d'une autre liste chainé de Personne.
 * \return Le concaténation des deux listes chainées passées en paramètre.
 */
Personne * concatListe(Personne * gauche, Personne * droite)
{
	if(gauche == NULL)
		return droite;

	Personne * tmp = gauche;
	while(tmp->personne_suiv != NULL)
		tmp = tmp->personne_suiv;
	tmp->personne_suiv = droite;

	return gauche;
}

/**
 * \fn Personne * quickSort(Personne * p)
 * \brief Fonction permettant de trier la liste chainée de Personne par age croissant.
 *
 * -> Fonction vu en cours.
 * \param p La tête de la liste chainée de Personne.
 * \return La nouvelle liste chainée de Personne une fois rangée, si la liste chainée passée en paramètre est NULL alors on retourne NULL.
 */
Personne * quickSort(Personne * p)
{
	if(p == NULL)
		return NULL;

	Personne * li = NULL;
	Personne * ls = NULL;
	Personne * premier = teteDeListe(p);
	
	for(Personne * reste = resteDeListe(p) ; reste != NULL ; reste = resteDeListe(reste))
	{
		Personne * nPremier = teteDeListe(reste);
		if(perscmp(premier, nPremier) < 0)
			ls = concatListe(ls, nPremier);
		else
			li = concatListe(li, nPremier);
	}

	li = quickSort(li);
	ls = quickSort(ls);

	Personne * resultat = concatListe(premier, ls);
	resultat = concatListe(li, resultat);

	return resultat;
}

//////////DICHOTOMIE//////////
/**
 * \fn int rechercheCode(int * base, int min, int max, int quoi)
 * \brief Fonction permettant d'effectuer une recherche par dichotomie récursif.
 *
 * -> Fonction vu en cours.
 * \param base Le tableau contenant des codes.
 * \param min la position minimum à respecter pour chercher le code.
 * \param max la position maximum à ne pas dépasser pour chercher le code.
 * \param quoi le code à chercher dans le tableau.
 * \return La position à laquelle le code(ici quoi) a été trouvé dans le tableau(ici base)
 */
int rechercheCode(int * base, int min, int max, int quoi)
{
	if(min >= max)
		return -1;

	int milieu = (max+min)/2;
	if(base[milieu] == quoi)
		return milieu;
	if(quoi < base[milieu])
		return rechercheCode(base, min, milieu-1, quoi);
	if(quoi > base[milieu])
		return rechercheCode(base, milieu+1, max, quoi);

	return -1;
}

//////////TRI A BULLE//////////
/**
 * \fn void triBase(int * base, int max)
 * \brief Fonction permettant d'effectuer un tri à bulle.
 *
 * -> Fonction vu en cours.
 * \param base Le tableau contenant des codes.
 * \param max la position maximum à ne pas dépasser pour chercher le code.
 */
void triBase(int * base, int max)
{
	while(max > 0)
	{
		int i = 0;

		while(i < max-1)
		{
			if(base[i] > base[i+1])
			{
				int temp = base[i];
				base[i] = base[i+1];
				base[i+1] = temp;
			}

			i += 1;
		}

		max -= 1;
	}
}

//////////FONCTIONS MENU//////////
//////////TRI A BULLE//////////
/**
 * \fn void afficherMenuPersonne(Donnee * donnee)
 * \brief Fonction permettant de faire appel au fonction associé à la gestion des Personnes sous forme d'un menu.
 *
 * \param donnee Une instance de la structure Donnee (!= NULL).
 */
void afficherMenuPersonne(Donnee * donnee)
{
	if(donnee == NULL)
		return;

	int choix;
	do
	{
		printf("\nMENU : Gestion des personnes\n");
		printf("0 - Quitter\n");
		printf("1 - Créer une nouvelle personne\n");
		printf("2 - Afficher les personnes\n");
		printf("3 - Ajouter un article dans le panier d'une personne\n");
		printf("4 - Payer le panier d'une personne\n");
		printf("5 - Ranger les personnes par age\n");
		printf("Choix : ");
		scanf("%d", &choix);
		printf("\n");

		switch(choix)
		{
			case 0:
			{
				printf("Retour au menu principal !\n");
				break;
			}
			case 1:
			{
				Personne * p = creerPersonne();
				donnee->personnes = ajoutPersonneDansChaine(donnee->personnes, p);
				break;
			}
			case 2:
			{
				printf("Affichages des personnes\n");
				afficherPersonne(donnee->personnes, 1, 1);
				break;
			}
			case 3:
			{
				printf("Ajout d'un article dans le panier d'une personne\n");
				printf("Personne :\n");
				Personne * personne = recherchePersonne(donnee->personnes);
				printf("Article :\n");
				Article * article = rechercheArticle(donnee->articles);
				ajoutArticleDansPanier(personne, article);
				break;
			}
			case 4:
			{
				printf("Passage en caisse d'une personne !\n");
				printf("Personne :\n");
				Personne * personne = recherchePersonne(donnee->personnes);
				passageEnCaisse(personne);
				break;
			}
			case 5:
			{
				printf("Rangement des personnes par age croissant !\n");
				donnee->personnes = quickSort(donnee->personnes);
				printf("Affichages des personnes rangées\n");
				afficherPersonne(donnee->personnes, 1, 0);
				break;
			}
			default:
			{
				printf("ERREUR : Mauvais choix !\n");
			}
		}
	} while(choix != 0);
}

/**
 * \fn void afficherMenuArticle(Donnee * donnee)
 * \brief Fonction permettant de faire appel au fonction associé à la gestion des Articles sous forme d'un menu.
 *
 * \param donnee Une instance de la structure Donnee (!= NULL).
 */
void afficherMenuArticle(Donnee * donnee)
{
	if(donnee == NULL)
		return;

	int choix;
	do
	{
		printf("\nMENU : Gestion des articles\n");
		printf("0 - Quitter\n");
		printf("1 - Créer un nouvel article\n");
		printf("2 - Afficher les articles\n");
		printf("3 - Vérifier l'existance d'un article\n");
		printf("Choix : ");
		scanf("%d", &choix);
		printf("\n");

		switch(choix)
		{
			case 0:
			{
				printf("Retour au menu principal !\n");
				break;
			}
			case 1:
			{
				Article * a = creerArticle();
				donnee->articles = ajoutArticleDansChaine(donnee->articles, a);
				ajouterArticleDansBase(donnee->bdd, a);
				break;
			}
			case 2:
			{
				printf("Affichages des articles\n");
				afficherArticle(donnee->articles, 1);
				break;
			}
			case 3:
			{
				printf("Vérification d'un article \n");
				verifierArticle(donnee->bdd);
				break;
			}
			default:
			{
				printf("ERREUR : Mauvais choix !\n");
			}
		}
	} while(choix != 0);
}

/**
 * \fn void afficherMenuMagasin(Donnee * donnee)
 * \brief Fonction permettant de faire appel au fonction associé à la gestion des Magasins sous forme d'un menu.
 *
 * \param donnee Une instance de la structure Donnee (!= NULL).
 */
void afficherMenuMagasin(Donnee * donnee)
{
	if(donnee == NULL)
		return;

	int choix;
	do
	{
		printf("\nMENU : Gestion des magasins\n");
		printf("0 - Quitter\n");
		printf("1 - Créer un nouveau magasin\n");
		printf("2 - Afficher les magasins\n");
		printf("3 - Vérifier l'existance d'un magasin\n");
		printf("4 - Ajouter un article dans un rayon du magasin\n");
		printf("5 - Afficher les rayons du magasin\n");
		printf("Choix : ");
		scanf("%d", &choix);
		printf("\n");

		switch(choix)
		{
			case 0:
			{
				printf("Retour au menu principal !\n");
				break;
			}
			case 1:
			{
				Magasin * m = creerMagasin();
				donnee->magasins = ajoutMagasinDansLaChaine(donnee->magasins, m);
				ajouterMagasinDansBase(donnee->bdd, m);
				break;
			}
			case 2:
			{
				printf("Affichages des magasins\n");
				afficherMagasin(donnee->magasins, 1);
				break;
			}
			case 3:
			{
				printf("Vérification d'un magasin \n");
				verifierMagasin(donnee->bdd);
				break;
			}
			case 4:
			{
				Magasin * m = rechercheMagasin(donnee->magasins);
				Article * a = rechercheArticle(donnee->articles);
				printf("Ajout d'un article dans un rayon du magasin n°%d : %s\n", m->code, m->nom);
				m->rayon = insererRayon(m->rayon, a);
				break;
			}
			case 5:
			{
				Magasin * m = rechercheMagasin(donnee->magasins);
				printf("Affichage des rayons du magasin n°%d : %s\n", m->code, m->nom);
				afficherRayon(m->rayon, 0, nombreDeRayon(m->rayon));
				break;
			}
			default:
			{
				printf("ERREUR : Mauvais choix !\n");
			}
		}
	} while(choix != 0);
}

/**
 * \fn void afficherMenuPrincipal(Donnee * donnee)
 * \brief Fonction permettant de faire appel aux principaux sous-menu sous forme d'un menu principal.
 *
 * \param donnee Une instance de la structure Donnee (!= NULL).
 */
void afficherMenuPrincipal(Donnee * donnee)
{
	if(donnee == NULL)
		return;

	int choix;
	do
	{
		printf("Application des techniques : Révision partiel informatique\n");
		printf("0 - Quitter\n");
		printf("1 - Gestion des personnes\n");
		printf("2 - Gestion des articles\n");
		printf("3 - Gestion des magasins\n");
		printf("Choix : ");
		scanf("%d", &choix);
		printf("\n");

		switch(choix)
		{
			case 0:
			{
				printf("Arret du programme !\n");
				arretProgramme(donnee);
				break;
			}
			case 1:
			{
				afficherMenuPersonne(donnee);
				break;
			}
			case 2:
			{
				afficherMenuArticle(donnee);
				break;
			}
			case 3:
			{
				afficherMenuMagasin(donnee);
				break;
			}
			default:
			{
				printf("ERREUR : Mauvais choix !\n");
			}
		}
	} while(choix != 0);
}

/**
 * \fn Donnee * initialisationProgramme()
 * \brief Fonction permettant d'initialiser les Données nécessaire au bon fonctionnement du programme.
 *
 * donnee->personnes, donnee->magasins, donnee->articles correspondent aux têtes des listes chainées.
 * Dans la fonction, on initialise la tête de la liste chainée des Personnes à NULL.
 * la tête de la liste chainée des Magasins à NULL.
 * la tête de la liste chainée des Articles à NULL.
 * L'initialisation de la base de donnée de Commerce se fait via la fonction creerBase().
 * \return Instance nouvelle allouée d'un objet de type Donnee (de programme) ou NULL.
 */
Donnee * initialisationProgramme()
{
	Donnee * donnee = (Donnee *) malloc(sizeof(Donnee));
	if(donnee == NULL)
	{
		printf("Erreur lors de l'allocation mémoire !\n");
		return NULL;
	}

	donnee->personnes = NULL;
	donnee->magasins = NULL;
	donnee->articles = NULL;
	donnee->bdd = creerBase();

	return donnee;
}

/**
 * \fn void arretProgramme(Donnee * donnee)
 * \brief Fonction permettant de libérer la mémoire du programme.
 *
 * La fonction supprime dans l'ordre toutes les personnes.
 * Puis tous les articles.
 * Puis tous les magasins.
 * Puis supprimer la base de donnée de commerce.
 * Et enfin libère l'espace mémoire associé aux données.
 * \param donnee Une instance de la structure Donnee (!= NULL).
 */
void arretProgramme(Donnee * donnee)
{
	if(donnee == NULL)
		return;

	supprimerPersonnes(donnee->personnes);
	supprimerArticles(donnee->articles);
	supprimerMagasins(donnee->magasins);
	supprimerBase(donnee->bdd);
	free(donnee);
	donnee = NULL;
}

/**
 * \fn int main(void)
 * \brief Entrée du programme.
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */
int main(void)
{
	Donnee * donnee = initialisationProgramme();
	if(donnee == NULL)
	{
		printf("ERREUR : Le programme ne peut pas être lancé !\n");
		return 1;
	}

	afficherMenuPrincipal(donnee);
	return EXIT_SUCCESS;
}
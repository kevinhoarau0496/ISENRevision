<h1>Programme de révision</h1>
<h1>A Propos</h1>
<p>
 * Auteur Kevin HOARAU<br>
 * Version 1.0<br>
 * Date 02 mai 2017<br>
</p>

<h1>Contact</h1>
<p>
<a href="mailto:kevinhoarau0496@gmail.com">kevinhoarau0496@gmail.com</a><br>
J'essaierai de vous répondre dans les meilleurs délais afin de vous aider.
</p>

<h1>Récupération du code</h1>
<p>Directement en téléchargement : <a href="../../revision.c">revision.c</a></p>
<p>Via Gitlab : <a href="https://gitlab.com/kevinhoarau0496/ISENRevision">ISENRevision</a></p>

<h1>A Savoir</h1>
<p>* Le code a été écrit avec l'aide des polycopiés acquis en cours afin de respecter au maximum ce qui est demandé.</p>
<p>* A cette version le programme est <b>stable</b> et <b>fonctionnel</b>.</p>
<p>* Il doit surement y avoir des fautes dans la documentation - désolé.</p>
<p>* Il est évidemment possible de modifier/améliorer le programme si vous le souhaitez, pensez à le redistribuer pour <b>conserver l'entraide</b>.</p>
<p>* Les méthodes de suppression et de modification des entitées : Personne, Article et Magasin n'ont pas été développé dans ce programme puisqu'ils ont été vus plusieurs fois en TP. En revanche il est possible de les rajouter</p> 

<h1>Compilation - Lancement</h1>
<h2>Compilation</h2>
<p>
Commande de compilation :<br>
<b>gcc revision.c -o revision -Wall --std=C99</b><br>
<span style="font-style: italic">Utiliser --std=c99 uniquement si votre compilateur utilise une version anterieure</span>
</p>
<h2>Lancement</h2>
<p>
Depuis le repertoire courant : <br>
./revision
</p>